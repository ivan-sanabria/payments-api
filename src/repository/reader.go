// Copyright 2022 Iván Camilo Sanabria - Santiago Florez
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package repository is responsible for handling transactions between database and s3 with exposed endpoints.
package repository

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"strconv"
)

// Model Define the meta struct to store data for clients.
type Model struct {
	Cashier   int64  `json:"cashier"`
	CloseTime int64  `json:"closeTime"`
	Data      []byte `json:"data"`
}

// GetCashierRecords implements the logic to retrieve an array of records from dynamoDB using specific hash and range key.
func GetCashierRecords(client dynamodbiface.DynamoDBAPI, table *string, hashKey int64, lowerRangeKey int64) ([]Model, error) {
	var results []Model

	input := &dynamodb.ScanInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":k": {
				N: aws.String(strconv.FormatInt(hashKey, 10)),
			},
			":lr": {
				N: aws.String(strconv.FormatInt(lowerRangeKey, 10)),
			},
		},
		FilterExpression: aws.String("cashier = :k and closeTime >= :lr"),
		TableName:        table,
	}

	output, err := client.Scan(input)
	if err != nil {
		return nil, err
	}

	if err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &results); err != nil {
		return nil, err
	}

	return results, err
}

// GetRangeCashierRecords implements the logic to retrieve an array of records from dynamoDB using specific range key.
func GetRangeCashierRecords(client dynamodbiface.DynamoDBAPI, table *string, lowerRangeKey string) ([]Model, error) {
	var results []Model

	input := &dynamodb.ScanInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":lr": {
				N: aws.String(lowerRangeKey),
			},
		},
		FilterExpression: aws.String("closeTime >= :lr"),
		TableName:        table,
	}

	output, err := client.Scan(input)
	if err != nil {
		return nil, err
	}

	if err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &results); err != nil {
		return nil, err
	}

	return results, err
}
