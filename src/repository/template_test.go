// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package repository is responsible for handling transactions between database and s3 with exposed endpoints.
package repository

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/mock"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
	"testing"
)

// TestGetModel implements the test case to validate the logic to load the json objects from s3.
func TestGetModel(t *testing.T) {
	bucket := "client"
	key := "efecty"
	data := `{"base":"1000"}`

	mockData := map[string]mock.Bucket{
		bucket: {
			key:          []byte(data),
			"additional": []byte(`{"sales":"5"}`),
		},
	}

	client := &mock.S3Client{Data: mockData}
	expected := aws.String(data)

	results, err := GetModel(client, &bucket, &key)
	assert.Nil(t, err)
	assert.Equal(t, results, expected)
}

// TestGetModelNotFound implements the test case to fail when the s3 objects are not found.
func TestGetModelNotFound(t *testing.T) {
	bucket := "client"
	key := "efecty"

	client := &mock.S3Client{}

	results, err := GetModel(client, &bucket, &key)
	assert.NotNil(t, err)
	assert.Nil(t, results)
}
