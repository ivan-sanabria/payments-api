// Copyright 2022 Iván Camilo Sanabria - Santiago Florez
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package repository is responsible for handling transactions between database and s3 with exposed endpoints.
package repository

import (
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

// PersistCashierRecord implements the logic to persist the given data into dynamoDB.
func PersistCashierRecord(client dynamodbiface.DynamoDBAPI, table *string, model Model) error {
	item, _ := dynamodbattribute.MarshalMap(model)

	input := &dynamodb.PutItemInput{
		Item:      item,
		TableName: table,
	}

	_, err := client.PutItem(input)

	return err
}
