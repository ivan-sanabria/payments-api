// Copyright 2022 Iván Camilo Sanabria - Santiago Florez
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package repository is responsible for handling transactions between database and s3 with exposed endpoints.
package repository

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/mock"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
	"testing"
)

// TestGetCashierRecords implements the test case to validate the logic to read an array of records from dynamoDB.
func TestGetCashierRecords(t *testing.T) {
	expected := []Model{
		{
			Cashier:   1,
			CloseTime: 12345,
			Data:      []byte(`{"withdraw":50000,"deposit":375000,"payment":187400,"debts":512400}`),
		},
	}

	client := &mock.DynamodbClient{}
	table := aws.String("efecty")

	hashKey := int64(1)
	lowerRange := int64(1)

	records, err := GetCashierRecords(client, table, hashKey, lowerRange)
	assert.Equal(t, expected, records)
	assert.Nil(t, err)
}

// TestGetCashierRecordsNotFound implements the test case to validate the logic to return empty results from dynamoDB.
func TestGetCashierRecordsNotFound(t *testing.T) {
	client := &mock.DynamodbClient{}
	table := aws.String("efecty")

	hashKey := int64(1)
	lowerRange := int64(123456)

	records, err := GetCashierRecords(client, table, hashKey, lowerRange)
	assert.Equal(t, []Model{}, records)
	assert.Nil(t, err)
}

// TestGetCashierRecordsConfigurationError implements the test case to fail when table is not given on input.
func TestGetCashierRecordsConfigurationError(t *testing.T) {
	client := &mock.DynamodbClient{}

	hashKey := int64(1)
	lowerRange := int64(1)

	records, err := GetCashierRecords(client, nil, hashKey, lowerRange)
	assert.Nil(t, records)
	assert.NotNil(t, err)
}

// TestGetCashierRecordsUnmarshallFailure implements the test case when the unmarshall statement fails.
func TestGetCashierRecordsUnmarshallFailure(t *testing.T) {
	client := &mock.DynamodbClient{}
	table := aws.String("unmarshall-error")

	hashKey := int64(1)
	lowerRange := int64(1)

	records, err := GetCashierRecords(client, table, hashKey, lowerRange)
	assert.Nil(t, records)
	assert.NotNil(t, err)
}

// TestGetRangeCashierRecords implements the test case to validate the logic to read an array of records from dynamoDB.
func TestGetRangeCashierRecords(t *testing.T) {
	expected := []Model{
		{
			Cashier:   1,
			CloseTime: 12345,
			Data:      []byte(`{"withdraw":50000,"deposit":375000,"payment":187400,"debts":512400}`),
		},
	}

	client := &mock.DynamodbClient{}
	table := aws.String("efecty")

	lowerRange := "1"

	records, err := GetRangeCashierRecords(client, table, lowerRange)
	assert.Equal(t, expected, records)
	assert.Nil(t, err)
}

// TestGetRangeCashierRecordsNotFound implements the test case to validate the logic to return empty results from dynamoDB.
func TestGetRangeCashierRecordsNotFound(t *testing.T) {
	client := &mock.DynamodbClient{}
	table := aws.String("efecty")

	lowerRange := "12346"

	records, err := GetRangeCashierRecords(client, table, lowerRange)
	assert.Equal(t, []Model{}, records)
	assert.Nil(t, err)
}

// TestGetRangeCashierRecordsConfigurationError implements the test case to fail when table is not given on input.
func TestGetRangeCashierRecordsConfigurationError(t *testing.T) {
	client := &mock.DynamodbClient{}

	lowerRange := "1"

	records, err := GetRangeCashierRecords(client, nil, lowerRange)
	assert.Nil(t, records)
	assert.NotNil(t, err)
}

// TestGetRangeCashierRecordsUnmarshallFailure implements the test case when the unmarshall statement fails.
func TestGetRangeCashierRecordsUnmarshallFailure(t *testing.T) {
	client := &mock.DynamodbClient{}
	table := aws.String("unmarshall-error")

	lowerRange := "1"

	records, err := GetRangeCashierRecords(client, table, lowerRange)
	assert.Nil(t, records)
	assert.NotNil(t, err)
}
