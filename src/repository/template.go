// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package repository is responsible for handling transactions between database and s3 with exposed endpoints.
package repository

import (
	"bytes"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
)

// GetModel implements the logic to retrieve model from s3.
func GetModel(client s3iface.S3API, bucket *string, key *string) (*string, error) {
	raw, err := client.GetObject(&s3.GetObjectInput{
		Bucket: bucket,
		Key:    key,
	})

	if err != nil {
		return nil, err
	}

	buf := new(bytes.Buffer)
	_, _ = buf.ReadFrom(raw.Body)

	return aws.String(buf.String()), err
}
