// Copyright 2022 Iván Camilo Sanabria - Santiago Florez
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package repository is responsible for handling transactions between database and s3 with exposed endpoints.
package repository

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/mock"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
	"testing"
)

// TestPersistCashierRecord implements the test case to validate the logic to persist the given data into dynamoDB.
func TestPersistCashierRecord(t *testing.T) {
	client := &mock.DynamodbClient{}
	table := aws.String("efecty")
	efecty := Model{
		Cashier:   1,
		CloseTime: 12345,
		Data:      []byte(`{"base":1000,"sales":10}`),
	}

	err := PersistCashierRecord(client, table, efecty)
	assert.Nil(t, err)
}

// TestPersistCashierRecordError implements the test case to fail when table is not given and data is not persisted.
func TestPersistCashierRecordError(t *testing.T) {
	client := &mock.DynamodbClient{}
	efecty := Model{
		Cashier:   1,
		CloseTime: 12345,
		Data:      []byte(`{"base":1000,"sales":10}`),
	}

	err := PersistCashierRecord(client, nil, efecty)
	assert.NotNil(t, err)
}
