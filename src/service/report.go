// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package service responsible for implementing the logic to generate general balance for the business.
package service

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/repository"
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
)

// Define constants for service layer.
const (
	Debit     = "debit"
	Credit    = "credit"
	Extension = ".json"
)

// GenerateDailyReport Generates the daily report for specific cashier.
func GenerateDailyReport(client dynamodbiface.DynamoDBAPI, cashier int64, closeTime int64, models []string,
	s3Client s3iface.S3API, bucket *string) (repository.Model, error) {

	summary := make(map[string]float64, 0)

	for i := range models {
		report, err := processModel(client, models[i], cashier, closeTime, s3Client, bucket)
		if err != nil {
			return repository.Model{}, err
		}
		for _, v := range report {
			summary[models[i]] = summary[models[i]] + v
		}
	}

	data, _ := json.Marshal(summary)

	return repository.Model{
		Cashier:   cashier,
		CloseTime: closeTime,
		Data:      data,
	}, nil
}

// processModel generate the report for a given model.
func processModel(client dynamodbiface.DynamoDBAPI, model string, cashier int64, closeTime int64,
	s3Client s3iface.S3API, bucket *string) (map[string]float64, error) {

	report := make(map[string]float64, 0)
	table := aws.String(model)
	template := aws.String(model + Extension)

	records, err := repository.GetCashierRecords(client, table, cashier, closeTime)
	if err != nil {
		return report, err
	}

	mapping := make(map[string]string, 0)
	result := make(map[string]float64, 0)

	for j := range records {
		err = json.Unmarshal(records[j].Data, &result)
		if err != nil {
			return report, err
		} else {
			err = processRecord(s3Client, bucket, template, mapping, report, result)
			if err != nil {
				return report, err
			}
		}
	}

	return report, nil
}

// processRecord map model definition in order to validate a record and put data into the report.
func processRecord(s3Client s3iface.S3API, bucket *string, template *string, mapping map[string]string,
	report map[string]float64, result map[string]float64) error {

	definition, err := repository.GetModel(s3Client, bucket, template)
	if err != nil {
		return err
	} else {
		err = json.Unmarshal([]byte(*definition), &mapping)
		if err != nil {
			return err
		}
		generateReport(mapping, report, result)
	}

	return nil
}

// generateReport add credits and debits.
func generateReport(mapping map[string]string, report map[string]float64, result map[string]float64) {
	for k, v := range mapping {
		if v == Credit {
			report[k] = report[k] - result[k]
		}
		if v == Debit {
			report[k] = report[k] + result[k]
		}
	}
}
