// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package service responsible for implementing the logic to generate general balance for the business.
package service

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/mock"
	"bitbucket.org/ivan-sanabria/payments-api/src/repository"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
	"testing"
)

// TestGenerateDailyReport implements the test case to validate balance for a model.
func TestGenerateDailyReport(t *testing.T) {
	expected := repository.Model{
		Cashier:   1,
		CloseTime: 12345,
		Data:      []byte(`{"efecty":0}`),
	}

	client := &mock.DynamodbClient{}
	cashier := int64(1)
	closeTime := int64(12345)

	models := []string{"efecty"}
	bucket := "models"
	mockData := map[string]mock.Bucket{
		bucket: {
			"efecty.json": []byte(`{"withdraw":"credit","deposit":"debit","payment":"debit","debts":"credit"}`),
		},
	}
	s3Client := &mock.S3Client{Data: mockData}

	result, err := GenerateDailyReport(client, cashier, closeTime, models, s3Client, aws.String(bucket))
	assert.Equal(t, expected, result)
	assert.Nil(t, err)
}

// TestGenerateDailyReportFailsGettingRecords implements the test case to validate error when dynamoDB fails.
func TestGenerateDailyReportFailsGettingRecords(t *testing.T) {
	expected := repository.Model{}

	client := &mock.DynamodbClient{}
	cashier := int64(1)
	closeTime := int64(12345)

	models := []string{""}
	bucket := "models"
	s3Client := &mock.S3Client{}

	result, err := GenerateDailyReport(client, cashier, closeTime, models, s3Client, aws.String(bucket))
	assert.Equal(t, expected, result)
	assert.NotNil(t, err)
}

// TestGenerateDailyReportFailsGettingModel implements the test case to validate error when reading model from S3 fails.
func TestGenerateDailyReportFailsGettingModel(t *testing.T) {
	expected := repository.Model{}

	client := &mock.DynamodbClient{}
	cashier := int64(1)
	closeTime := int64(12345)

	models := []string{"efecty"}

	bucket := "models"
	mockData := map[string]mock.Bucket{
		bucket: {
			"other.json": []byte(`{"withdraw":"credit","deposit":"debit","payment":"debit","debts":"credit"}`),
		},
	}
	s3Client := &mock.S3Client{Data: mockData}

	result, err := GenerateDailyReport(client, cashier, closeTime, models, s3Client, aws.String(bucket))
	assert.Equal(t, expected, result)
	assert.NotNil(t, err)
}

// TestGenerateDailyReportFailsParsingModel implements the test case to validate error when parsing model from S3 fails.
func TestGenerateDailyReportFailsParsingModel(t *testing.T) {
	expected := repository.Model{}

	client := &mock.DynamodbClient{}
	cashier := int64(1)
	closeTime := int64(12345)

	models := []string{"efecty"}

	bucket := "models"
	mockData := map[string]mock.Bucket{
		bucket: {
			"efecty.json": []byte(`{This is not a json}`),
		},
	}
	s3Client := &mock.S3Client{Data: mockData}

	result, err := GenerateDailyReport(client, cashier, closeTime, models, s3Client, aws.String(bucket))
	assert.Equal(t, expected, result)
	assert.NotNil(t, err)
}

// TestGenerateDailyReportFailsErrorDataType implements the test case to validate error when type of data is unexpected.
func TestGenerateDailyReportFailsErrorDataType(t *testing.T) {
	expected := repository.Model{}

	client := &mock.DynamodbClient{}
	cashier := int64(1)
	closeTime := int64(12345)

	models := []string{"efecty_type"}

	bucket := "models"
	mockData := map[string]mock.Bucket{
		bucket: {
			"efecty_type.json": []byte(`{"withdraw":"credit","deposit":"debit","payment":"debit","debts":"credit"}`),
		},
	}
	s3Client := &mock.S3Client{Data: mockData}

	result, err := GenerateDailyReport(client, cashier, closeTime, models, s3Client, aws.String(bucket))
	assert.Equal(t, expected, result)
	assert.NotNil(t, err)
}
