// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package service responsible for implementing the logic to generate general balance for the business.
package service

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/repository"
	"encoding/json"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

// GenerateBalanceReport Generates the monthly report for the business.
func GenerateBalanceReport(client dynamodbiface.DynamoDBAPI, model *string, supervisor int64, closeTime int64) (repository.Model, error) {

	records, err := repository.GetCashierRecords(client, model, supervisor, closeTime)
	if err != nil {
		return repository.Model{}, err
	}

	result := make(map[string]float64, 0)
	report := make(map[string]float64, 0)

	for i := range records {
		err = json.Unmarshal(records[i].Data, &result)
		if err != nil {
			return repository.Model{}, err
		}

		for k, v := range result {
			report[k] = report[k] + v
		}
	}

	data, _ := json.Marshal(report)

	return repository.Model{
		Cashier:   supervisor,
		CloseTime: closeTime,
		Data:      data,
	}, nil
}
