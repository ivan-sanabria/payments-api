// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package service responsible for implementing the logic to generate general balance for the business.
package service

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/mock"
	"bitbucket.org/ivan-sanabria/payments-api/src/repository"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
	"testing"
)

// TestGenerateBalanceReport implements the test case to validate the generation of the balance.
func TestGenerateBalanceReport(t *testing.T) {
	expected := repository.Model{
		Cashier:   1,
		CloseTime: 12345,
		Data:      []byte(`{"additionals":95000,"efecty":18000}`),
	}

	client := &mock.DynamodbClient{}
	cashier := int64(1)
	closeTime := int64(12345)

	model := aws.String("daily")

	result, err := GenerateBalanceReport(client, model, cashier, closeTime)
	assert.Equal(t, expected, result)
	assert.Nil(t, err)
}

// TestGenerateBalanceReportFailsGettingRecords implements the test case to validate error when dynamoDB fails.
func TestGenerateBalanceReportFailsGettingRecords(t *testing.T) {
	expected := repository.Model{}

	client := &mock.DynamodbClient{}
	cashier := int64(1)
	closeTime := int64(12345)

	result, err := GenerateBalanceReport(client, nil, cashier, closeTime)
	assert.Equal(t, expected, result)
	assert.NotNil(t, err)
}

// TestGenerateBalanceReportFailsErrorDataType implements the test case to validate error when type of data is unexpected.
func TestGenerateBalanceReportFailsErrorDataType(t *testing.T) {
	expected := repository.Model{}

	client := &mock.DynamodbClient{}
	cashier := int64(1)
	closeTime := int64(12345)

	model := aws.String("daily_type")

	result, err := GenerateBalanceReport(client, model, cashier, closeTime)
	assert.Equal(t, expected, result)
	assert.NotNil(t, err)
}
