// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package handler implements the logic to server request for specific use case.
package handler

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/mock"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

// TestGenerateReport implements the test to validate the response after generating the daily cashier report.
func TestGenerateReport(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `{"cashier":1,"closeTime":12345,"data":{"efecty":0}}`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	cashier := "1"
	closeTime := "12345"

	models := []string{"efecty"}
	bucket := "models"
	mockData := map[string]mock.Bucket{
		bucket: {
			"efecty.json": []byte(`{"withdraw":"credit","deposit":"debit","payment":"debit","debts":"credit"}`),
		},
	}
	s3Client := &mock.S3Client{Data: mockData}

	response := GenerateReport(client, cashier, closeTime, models, s3Client, aws.String(bucket))
	assert.Equal(t, expected, response)
}

// TestGenerateReportCashierError implements the test to validate the response after an invalid cashier is given.
func TestGenerateReportCashierError(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `strconv.ParseInt: parsing "This is not a number": invalid syntax`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	cashier := "This is not a number"
	closeTime := "12345"

	models := []string{"efecty"}
	bucket := "quotes-test"
	mockData := map[string]mock.Bucket{}
	s3Client := &mock.S3Client{Data: mockData}

	response := GenerateReport(client, cashier, closeTime, models, s3Client, aws.String(bucket))
	assert.Equal(t, expected, response)
}

// TestGenerateReportCloseTimeError implements the test to validate the response after an invalid close time is given.
func TestGenerateReportCloseTimeError(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `strconv.ParseInt: parsing "This is not a unix time": invalid syntax`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	cashier := "1"
	closeTime := "This is not a unix time"

	models := []string{"efecty"}
	bucket := "quotes-test"
	mockData := map[string]mock.Bucket{}
	s3Client := &mock.S3Client{Data: mockData}

	response := GenerateReport(client, cashier, closeTime, models, s3Client, aws.String(bucket))
	assert.Equal(t, expected, response)
}

// TestGenerateReportNotModelGiven implements the test to validate the response after invalid model array is given.
func TestGenerateReportNotModelGiven(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `missing required field Scan.TableName`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	cashier := "1"
	closeTime := "12345"

	models := []string{""}
	bucket := "quotes-test"
	mockData := map[string]mock.Bucket{}
	s3Client := &mock.S3Client{Data: mockData}

	response := GenerateReport(client, cashier, closeTime, models, s3Client, aws.String(bucket))
	assert.Equal(t, expected, response)
}

// TestGenerateReportModelNotFound implements the test to validate the response after model is not found in s3.
func TestGenerateReportModelNotFound(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `missing key at get object function`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	cashier := "1"
	closeTime := "12345"

	models := []string{"efecty"}
	bucket := "key-not-exists"
	mockData := map[string]mock.Bucket{
		"models": {
			"efecty.json": []byte(`{"withdraw":"credit","deposit":"debit","payment":"debit","debts":"credit"}`),
		},
	}
	s3Client := &mock.S3Client{Data: mockData}

	response := GenerateReport(client, cashier, closeTime, models, s3Client, aws.String(bucket))
	assert.Equal(t, expected, response)
}
