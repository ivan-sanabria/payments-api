// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package handler implements the logic to server request for specific use case.
package handler

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/service"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"net/http"
	"strconv"
)

// GenerateReport implements the logic to generate daily report for specific cashier.
func GenerateReport(client dynamodbiface.DynamoDBAPI, cashier string, closeTime string, models []string,
	s3Client s3iface.S3API, bucket *string) events.APIGatewayProxyResponse {

	parsedCashier, err := strconv.ParseInt(cashier, 10, 64)
	if err != nil {
		return buildErrorResponse(err)
	}

	parsedCloseTime, err := strconv.ParseInt(closeTime, 10, 64)
	if err != nil {
		return buildErrorResponse(err)
	}

	result, err := service.GenerateDailyReport(client, parsedCashier, parsedCloseTime, models, s3Client, bucket)
	if err != nil {
		return buildErrorResponse(err)
	}

	return buildApiResponse(http.StatusOK, buildBody(result))
}
