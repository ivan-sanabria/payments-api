// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package handler implements the logic to server request for specific use case.
package handler

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/service"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"net/http"
	"strconv"
)

// GenerateBalance implements the logic to generate daily report for specific cashier.
func GenerateBalance(client dynamodbiface.DynamoDBAPI, table *string, supervisor string, closeTime string) events.APIGatewayProxyResponse {
	parsedSupervisor, err := strconv.ParseInt(supervisor, 10, 64)
	if err != nil {
		return buildErrorResponse(err)
	}

	parsedCloseTime, err := strconv.ParseInt(closeTime, 10, 64)
	if err != nil {
		return buildErrorResponse(err)
	}

	result, err := service.GenerateBalanceReport(client, table, parsedSupervisor, parsedCloseTime)
	if err != nil {
		return buildErrorResponse(err)
	}

	return buildApiResponse(http.StatusOK, buildBody(result))
}
