// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package handler implements the logic to server request for specific use case.
package handler

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/mock"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

// TestGenerateBalance implements the test to validate the response after generating the general balance.
func TestGenerateBalance(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `{"cashier":1,"closeTime":12345,"data":{"additionals":95000,"efecty":18000}}`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	table := aws.String("daily")
	cashier := "1"
	closeTime := "12345"

	response := GenerateBalance(client, table, cashier, closeTime)
	assert.Equal(t, expected, response)
}

// TestGenerateBalanceCashierError implements the test to validate the response after an invalid cashier is given.
func TestGenerateBalanceCashierError(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `strconv.ParseInt: parsing "This is not a number": invalid syntax`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	table := aws.String("daily")
	cashier := "This is not a number"
	closeTime := "12345"

	response := GenerateBalance(client, table, cashier, closeTime)
	assert.Equal(t, expected, response)
}

// TestGenerateBalanceCloseTimeError implements the test to validate the response after an invalid close time is given.
func TestGenerateBalanceCloseTimeError(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `strconv.ParseInt: parsing "This is not a unix time": invalid syntax`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	table := aws.String("daily")
	cashier := "1"
	closeTime := "This is not a unix time"

	response := GenerateBalance(client, table, cashier, closeTime)
	assert.Equal(t, expected, response)
}

// TestGenerateBalanceNotModelGiven implements the test to validate the response after configuration of table is missing.
func TestGenerateBalanceNotModelGiven(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `missing required field Scan.TableName`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	cashier := "1"
	closeTime := "12345"

	response := GenerateBalance(client, nil, cashier, closeTime)
	assert.Equal(t, expected, response)
}
