// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package handler implements the logic to server request for specific use case.
package handler

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/repository"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"net/http"
	"strconv"
)

// GetCashierRecords retrieves a specific model from dynamoDB using the given table and hash key.
func GetCashierRecords(client dynamodbiface.DynamoDBAPI, table *string, cashier string, closeTime string) events.APIGatewayProxyResponse {
	parsedCashier, err := strconv.ParseInt(cashier, 10, 64)
	if err != nil {
		return buildErrorResponse(err)
	}

	parsedCloseTime, err := strconv.ParseInt(closeTime, 10, 64)
	if err != nil {
		return buildErrorResponse(err)
	}

	results, err := repository.GetCashierRecords(client, table, parsedCashier, parsedCloseTime)
	if err != nil {
		return buildErrorResponse(err)
	}

	return buildApiResponse(http.StatusOK, buildArrayBody(results))
}

// PersistCashierRecord stores a specific model into dynamoDB using the given table and data.
func PersistCashierRecord(client dynamodbiface.DynamoDBAPI, table *string, cashier string, closeTime string, body string) events.APIGatewayProxyResponse {
	var rawBody json.RawMessage

	err := json.Unmarshal([]byte(body), &rawBody)
	if err != nil {
		return buildErrorResponse(err)
	}

	parsedCashier, err := strconv.ParseInt(cashier, 10, 64)
	if err != nil {
		return buildErrorResponse(err)
	}

	parsedCloseTime, err := strconv.ParseInt(closeTime, 10, 64)
	if err != nil {
		return buildErrorResponse(err)
	}

	model := repository.Model{
		Cashier:   parsedCashier,
		CloseTime: parsedCloseTime,
		Data:      rawBody,
	}

	if err = repository.PersistCashierRecord(client, table, model); err != nil {
		return buildErrorResponse(err)
	}

	return buildApiResponse(http.StatusCreated, EmptyBody)
}
