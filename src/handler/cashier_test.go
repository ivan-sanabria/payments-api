// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package handler implements the logic to server request for specific use case.
package handler

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/mock"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

// TestGetCashierRecords implements the test case to validate get models from dynamoDB.
func TestGetCashierRecords(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `[{"cashier":1,"closeTime":12345,"data":{"withdraw":50000,"deposit":375000,"payment":187400,"debts":512400}}]`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	table := aws.String("efecty")
	cashier := "1"
	closeTime := "12345"

	response := GetCashierRecords(client, table, cashier, closeTime)
	assert.Equal(t, expected, response)
}

// TestGetCashierRecordsWrongCashier implements the test case to validate the cashier parameter is valid.
func TestGetCashierRecordsWrongCashier(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `strconv.ParseInt: parsing "This is not a number": invalid syntax`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	table := aws.String("efecty")
	cashier := "This is not a number"
	closeTime := "12345"

	response := GetCashierRecords(client, table, cashier, closeTime)
	assert.Equal(t, expected, response)
}

// TestGetCashierRecordsWrongCloseTime implements the test case to validate the close time parameter is valid.
func TestGetCashierRecordsWrongCloseTime(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `strconv.ParseInt: parsing "This is not a number": invalid syntax`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	table := aws.String("efecty")
	cashier := "1"
	closeTime := "This is not a number"

	response := GetCashierRecords(client, table, cashier, closeTime)
	assert.Equal(t, expected, response)
}

// TestGetCashierRecordsError implements the test case when service fails.
func TestGetCashierRecordsError(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body: "missing required field Scan.TableName",
	}

	client := &mock.DynamodbClient{}
	cashier := "12345"
	closeTime := "12345"

	response := GetCashierRecords(client, nil, cashier, closeTime)
	assert.Equal(t, expected, response)
}

// TestPersistCashierRecord implements the test case to validate put model into dynamoDB.
func TestPersistCashierRecord(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusCreated,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
	}

	client := &mock.DynamodbClient{}
	model := aws.String("efecty")
	cashier := "1"
	closeTime := "12345"
	body := `{"base":1000,"sales":10}`

	response := PersistCashierRecord(client, model, cashier, closeTime, body)
	assert.Equal(t, expected, response)
}

// TestPersistCashierWrongCashier implements the test case to validate the cashier parameter is valid.
func TestPersistCashierWrongCashier(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `strconv.ParseInt: parsing "This is not a number": invalid syntax`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	model := aws.String("efecty")
	cashier := "This is not a number"
	closeTime := "12345"
	body := `{"base":1000,"sales":10}`

	response := PersistCashierRecord(client, model, cashier, closeTime, body)
	assert.Equal(t, expected, response)
}

// TestPersistCashierWrongCloseTime implements the test case to validate the cashier parameter is valid.
func TestPersistCashierWrongCloseTime(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            `strconv.ParseInt: parsing "This is not a number": invalid syntax`,
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	model := aws.String("efecty")
	cashier := "1"
	closeTime := "This is not a number"
	body := `{"base":1000,"sales":10}`

	response := PersistCashierRecord(client, model, cashier, closeTime, body)
	assert.Equal(t, expected, response)
}

// TestPersistCashierRecordBodyError implements the test case when the given body is not json.
func TestPersistCashierRecordBodyError(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            "unexpected end of JSON input",
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	model := aws.String("efecty")
	cashier := "1"
	closeTime := "12345"

	response := PersistCashierRecord(client, model, cashier, closeTime, "")
	assert.Equal(t, expected, response)
}

// TestPersistCashierRecordError implements the test case when the service fails due to a configuration.
func TestPersistCashierRecordError(t *testing.T) {
	expected := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     "Content-Type",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			"Content-Type":                     "application/json",
		},
		Body:            "missing required field PutItemInput.TableName",
		IsBase64Encoded: false,
	}

	client := &mock.DynamodbClient{}
	model := aws.String("")
	cashier := "1"
	closeTime := "12345"
	body := `{}`

	response := PersistCashierRecord(client, model, cashier, closeTime, body)
	assert.Equal(t, expected, response)
}
