// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package handler implements the logic to server request for specific domain.
package handler

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/repository"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"net/http"
)

// Define constants for response builder.
const (
	EmptyBody     = ""
	ContentHeader = "Content-Type"
)

// Payload Define the meta struct to transfer information to api clients.
type Payload struct {
	Cashier   int64           `json:"cashier"`
	CloseTime int64           `json:"closeTime"`
	Data      json.RawMessage `json:"data"`
}

// buildBody creates body embedded in events.APIGatewayProxyResponse for an item response.
func buildBody(result repository.Model) string {

	payload := Payload{
		Cashier:   result.Cashier,
		CloseTime: result.CloseTime,
		Data:      result.Data,
	}

	out, _ := json.Marshal(payload)

	return string(out)
}

// buildArrayBody creates body embedded in events.APIGatewayProxyResponse for an array response.
func buildArrayBody(results []repository.Model) string {

	response := make([]Payload, 0)

	for i := range results {

		payload := Payload{
			Cashier:   results[i].Cashier,
			CloseTime: results[i].CloseTime,
			Data:      results[i].Data,
		}

		response = append(response, payload)
	}

	out, _ := json.Marshal(response)

	return string(out)
}

// buildApiResponse creates a new instance of events.APIGatewayProxyResponse using the given http code and payload.
func buildApiResponse(httpCode int, body string) events.APIGatewayProxyResponse {
	return events.APIGatewayProxyResponse{
		StatusCode: httpCode,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     ContentHeader,
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			ContentHeader:                      "application/json",
		},
		Body:            body,
		IsBase64Encoded: false,
	}
}

// buildErrorResponse creates a new instance of events.APIGatewayProxyResponse using the error.
func buildErrorResponse(err error) events.APIGatewayProxyResponse {
	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Headers: map[string]string{
			"Access-Control-Allow-Headers":     ContentHeader,
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
			ContentHeader:                      "application/json",
		},
		Body: err.Error(),
	}
}
