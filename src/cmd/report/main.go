// Copyright 2022 Iván Camilo Sanabria - Santiago Florez
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package main implements the function to expose the API to handle daily report data.
package main

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/handler"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/s3"
	"log"
	"os"
	"strings"
	"time"
)

// getS3Client retrieves a new instance of S3 client to read objects from S3 bucket.
func getS3Client() *s3.S3 {
	s3session, _ := session.NewSession(&aws.Config{
		Region: aws.String(os.Getenv("REGION"))},
	)

	return s3.New(s3session)
}

// getDynamoDBClient retrieves a new instance of dynamoDB client to persist or retrieve information from dynamoDB. This
// method should be used in each main of the cmd subpackages in order to have independent clients.
func getDynamoDBClient() *dynamodb.DynamoDB {
	dynamodbSession := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	return dynamodb.New(dynamodbSession)
}

// ProxyRequest implements the call the right function base on the HTTP method of the request.
func ProxyRequest(request events.APIGatewayProxyRequest) events.APIGatewayProxyResponse {
	start := time.Now()
	response := events.APIGatewayProxyResponse{}

	client := getDynamoDBClient()
	configuration := os.Getenv("SUPPORTED_MODELS")
	models := strings.Split(configuration, ",")

	s3Client := getS3Client()
	bucket := aws.String(os.Getenv("MODEL_BUCKET"))

	if request.HTTPMethod == "GET" {
		cashier := request.PathParameters["cashier"]
		closeTime := request.PathParameters["closeTime"]
		response = handler.GenerateReport(client, cashier, closeTime, models, s3Client, bucket)
	}

	elapsed := time.Since(start)
	log.Printf("Request was processed successfully and elapsed runtime was %s.", elapsed)

	return response
}

// LambdaHandler implements the function that handle requests made to /report endpoint.
func LambdaHandler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	return ProxyRequest(request), nil
}

// main function that trigger the lambda in order to receive requests on the expose API endpoints.
func main() {
	lambda.Start(LambdaHandler)
}
