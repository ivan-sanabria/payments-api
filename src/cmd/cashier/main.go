// Copyright 2022 Iván Camilo Sanabria - Santiago Florez
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package main implements the function to expose the API to handle cashier data.
package main

import (
	"bitbucket.org/ivan-sanabria/payments-api/src/handler"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"log"
	"time"
)

// getDynamoDBClient retrieves a new instance of dynamoDB client to persist or retrieve information from dynamoDB.
// This method should be used in each main of the cmd subpackages in order to have independent clients.
func getDynamoDBClient() *dynamodb.DynamoDB {
	dynamodbSession := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	return dynamodb.New(dynamodbSession)
}

// ProxyRequest implements the call the right function base on the HTTP method of the request.
func ProxyRequest(request events.APIGatewayProxyRequest) events.APIGatewayProxyResponse {
	start := time.Now()
	response := events.APIGatewayProxyResponse{}

	client := getDynamoDBClient()

	table := aws.String(request.PathParameters["model"])
	cashier := request.PathParameters["cashier"]
	closeTime := request.PathParameters["closeTime"]

	if request.HTTPMethod == "GET" {
		response = handler.GetCashierRecords(client, table, cashier, closeTime)
	} else {
		response = handler.PersistCashierRecord(client, table, cashier, closeTime, request.Body)
	}

	elapsed := time.Since(start)
	log.Printf("Request was processed successfully and elapsed runtime was %s.", elapsed)

	return response
}

// LambdaHandler implements the function that handle requests made to /model endpoint.
func LambdaHandler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	return ProxyRequest(request), nil
}

// main function that trigger the lambda in order to receive requests on the expose API endpoints.
func main() {
	lambda.Start(LambdaHandler)
}
