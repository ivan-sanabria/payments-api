// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package mock is responsible for providing mocks for specific services used in the api.
package mock

import (
	"bytes"
	"errors"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"io"
)

// Bucket is local map to simulate S3 bucket in unit tests.
type Bucket map[string][]byte

// S3Client Defines a mock struct to use as s3 client in unit tests.
type S3Client struct {
	s3iface.S3API
	Data map[string]Bucket
}

// GetObject is mock function used on unit tests.
func (m *S3Client) GetObject(input *s3.GetObjectInput) (*s3.GetObjectOutput, error) {
	bucket := m.Data[*input.Bucket]

	if object, ok := bucket[*input.Key]; ok {
		body := io.NopCloser(bytes.NewReader(object))
		output := s3.GetObjectOutput{
			Body: body,
		}
		return &output, nil
	} else {
		return nil, errors.New("missing key at get object function")
	}
}
