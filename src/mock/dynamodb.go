// Copyright 2022 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package mock is responsible for providing mocks for specific services used in the api.
package mock

import (
	"errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"strconv"
)

// DynamodbClient Defines a mock struct to use as dynamo client in unit tests.
type DynamodbClient struct {
	dynamodbiface.DynamoDBAPI
}

// Scan is mock function used to unit tests.
func (m *DynamodbClient) Scan(input *dynamodb.ScanInput) (*dynamodb.ScanOutput, error) {
	result := dynamodb.ScanOutput{}
	closeTime := 12345

	if input.TableName == nil || *input.TableName == "" {
		return &result, errors.New("missing required field Scan.TableName")
	}

	lr, _ := strconv.Atoi(*input.ExpressionAttributeValues[":lr"].N)

	if *input.TableName == "efecty" && closeTime >= lr {
		result.Items = []map[string]*dynamodb.AttributeValue{
			{
				"cashier": {
					N: aws.String("1"),
				},
				"closeTime": {
					N: aws.String(strconv.Itoa(closeTime)),
				},
				"data": {
					B: []byte(`{"withdraw":50000,"deposit":375000,"payment":187400,"debts":512400}`),
				},
			},
		}
	}

	if *input.TableName == "efecty_type" && closeTime >= lr {
		result.Items = []map[string]*dynamodb.AttributeValue{
			{
				"cashier": {
					N: aws.String("1"),
				},
				"closeTime": {
					N: aws.String(strconv.Itoa(closeTime)),
				},
				"data": {
					B: []byte(`"This is not a map of numbers"`),
				},
			},
		}
	}

	if *input.TableName == "daily" && closeTime >= lr {
		result.Items = []map[string]*dynamodb.AttributeValue{
			{
				"cashier": {
					N: aws.String("1"),
				},
				"closeTime": {
					N: aws.String(strconv.Itoa(closeTime)),
				},
				"data": {
					B: []byte(`{"efecty":30000,"additionals":25000}`),
				},
			},
			{
				"cashier": {
					N: aws.String("1"),
				},
				"closeTime": {
					N: aws.String(strconv.Itoa(closeTime)),
				},
				"data": {
					B: []byte(`{"efecty":-12000,"additionals":70000}`),
				},
			},
		}
	}

	if *input.TableName == "daily_type" && closeTime >= lr {
		result.Items = []map[string]*dynamodb.AttributeValue{
			{
				"cashier": {
					N: aws.String("1"),
				},
				"closeTime": {
					N: aws.String(strconv.Itoa(closeTime)),
				},
				"data": {
					B: []byte(`"This is not a map of numbers"`),
				},
			},
		}
	}

	if *input.TableName == "unmarshall-error" {
		result.Items = []map[string]*dynamodb.AttributeValue{
			{
				"cashier": {
					S: aws.String("1"),
				},
				"closeTime": {
					S: aws.String("12345"),
				},
				"data": {
					S: aws.String(`{"base":1000}`),
				},
			},
		}
	}

	return &result, nil
}

// PutItem is mock function used to unit tests.
func (m *DynamodbClient) PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {
	result := dynamodb.PutItemOutput{}

	if input.TableName == nil || *input.TableName == "" {
		return &result, errors.New("missing required field PutItemInput.TableName")
	}

	return &result, nil
}
