# Payments API

version 1.2.0 - 18/01/2023

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/payments-api.svg)](http://bitbucket.org/ivan-sanabria/payments-api/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/payments-api.svg)](http://bitbucket.org/ivan-sanabria/payments-api/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_payments-api&metric=alert_status)](https://sonarcloud.io/component_measures/metric/alert_status/list?id=ivan-sanabria_payments-api)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_payments-api&metric=bugs)](https://sonarcloud.io/component_measures/metric/bugs/list?id=ivan-sanabria_payments-api)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_payments-api&metric=coverage)](https://sonarcloud.io/component_measures/metric/coverage/list?id=ivan-sanabria_payments-api)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_payments-api&metric=ncloc)](https://sonarcloud.io/component_measures/metric/ncloc/list?id=ivan-sanabria_payments-api)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_payments-api&metric=sqale_index)](https://sonarcloud.io/component_measures/metric/sqale_index/list?id=ivan-sanabria_payments-api)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_payments-api&metric=vulnerabilities)](https://sonarcloud.io/component_measures/metric/vulnerabilities/list?id=ivan-sanabria_payments-api)

## Requirements
- Golang 1.17.x
- Serverless 1.83.x

## Serverless Setup

In order to deploy the application in AWS account is required to have an **AWS_ACCESS_KEY_ID**
and **AWS_SECRET_ACCESS_KEY**.

To setup and configure serverless framework, for deploying the stack using AWS CloudFormation is
required to execute the following commands on the root folder of the project on a terminal:

```bash
    npm install -g serverless
    serverless config credentials --provider aws --key ${AWS_ACCESS_KEY_ID} --secret ${AWS_SECRET_ACCESS_KEY} --profile demos
```

## Check Application Test Coverage

To verify test coverage on terminal:

1. Verify the version of Go - 1.17.x or higher.
2. Download the source code from repository.
3. Open a terminal.
4. Go to the root location of the source code.
5. Execute the commands:

```bash
    go test ./... -coverprofile=coverage.out
```

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of Go - 1.17.x or higher.
2. Download the source code from repository.
3. Open a terminal.
4. Go to the root location of the source code.
5. Execute the commands:

```bash
    go install  github.com/google/go-licenses
    go-licenses csv ./...
```

## Deployment on AWS using Serverless

After configuring serverless, see the test coverage, licenses and generated files, let's proceed to deploy
the application in AWS Lambda to serve requests:

```bash
    GOARCH=amd64 GOOS=linux go build -o bin ./...
    serverless deploy -v --aws-profile demos
```

## Test Application in Production

After deployment is successful, there are 4 endpoints exposed:

- POST - https://${aws-temporal-domain}/{model}
- GET  - https://${aws-temporal-domain}/{model}/{cashier}/{closeTime}
- GET  - https://${aws-temporal-domain}/report/{cashier}/{closeTime}
- GET  - https://${aws-temporal-domain}/balance/{supervisor}/{closeTime}

To test the endpoints, you could use the following commands:

```bash
    curl -X POST -d '{"cashier":1,"closeTime":1657654636,"data":{"base":1000,"sales":10}}' https://${aws-temporal-domain}/{model}
    curl -X GET https://${aws-temporal-domain}/{model}/{cashier}/{closeTime}
    curl -X GET https://${aws-temporal-domain}/report/{cashier}/{closeTime}
    curl -X GET https://${aws-temporal-domain}/balance/{supervisor}/{closeTime}
```

# Contact Information

Email: icsanabriar@googlemail.com
